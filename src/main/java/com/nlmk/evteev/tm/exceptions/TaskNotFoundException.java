package com.nlmk.evteev.tm.exceptions;

public class TaskNotFoundException extends Exception {

    /**
     * Ошибка при поиске задачи
     *
     * @param errorMsg строка сообщени об ошибке
     */
    public TaskNotFoundException(String errorMsg) {
        super(errorMsg);
    }
}
