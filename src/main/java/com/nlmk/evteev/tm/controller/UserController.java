package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.UserService;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Класс контроллера для работы с пользователем
 */
public class UserController extends AbstractController {

    private final Logger logger = LogManager.getLogger(UserController.class);
    private static UserController instance;
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);
    private User appUser;


    /**
     * Создает статический экземпляр класса
     *
     * @return экземпляр класса {@link UserController}
     */
    public static UserController getInstance() {
        synchronized (UserController.class) {
            return instance == null ? instance = new UserController() : instance;
        }
    }

    public User getAppUser() {
        return appUser;
    }

    public void setAppUser(User appUser) {
        this.appUser = appUser;
    }

    /**
     * Создание пользователя
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param loginName  логон пользователя
     * @param password   пароль
     * @param userRole   признак администратора
     * @return код исполнения
     */
    public int createUser
    (
            final String loginName, final String password, final UserRole userRole,
            final String firstName, final String middleName, final String secondName
    ) {
        userService.create(loginName, password, userRole, firstName, middleName, secondName);
        return 0;
    }

    /**
     * Создание пользователя в интерактивном режиме
     *
     * @return код исполнения
     */
    public int createUser() {
        logger.info("[CREATE USER]");
        System.out.println("Введите регистрационное имя:");
        final String loginName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(loginName))
            return TaskManagerUtil.printAndReturnFail("Регистрационное имя не может быть пустым!");
        if (userService.getUserByloginName(loginName) != null)
            return TaskManagerUtil.printAndReturnFail("Пользователь с таким регистрационным именем уже существует!");
        System.out.println("Введите пароль:");
        final String password = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(password))
            return TaskManagerUtil.printAndReturnFail("Пароль не может быть пустым!");
        final UserRole userRole = addAdminRole();
        userService.create(loginName, password, userRole);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Запрос на предоставление прав адинистратора
     *
     * @return роль пользователя {@link UserRole}
     */
    private UserRole addAdminRole() {
        System.out.println("Предоставить административные права? (y - да, n - нет):");
        final String s = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(s)) return UserRole.USER;
        return s.trim().equals("y") ? UserRole.ADMIN : UserRole.USER;
    }

    /**
     * Изменение пользователя
     *
     * @return код исполнения
     */
    public int updateUser() {
        logger.info("[UPDATE USER DATA]");
        System.out.println("Введите регистрационное имя пользователя:");
        final String loginName = scanner.nextLine();
        User user = userService.getUserByloginName(loginName);
        if (user == null)
            return TaskManagerUtil.printAndReturnFail("Пользователя с таким регистрационным именем не существует.");
        inputFIO(user);
        final UserRole userRole = addAdminRole();
        user.setUserRole(userRole);
        userService.update(user);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Ввод данных о пользователе
     *
     * @param user пользователь
     */
    private void inputFIO(User user) {
        System.out.println("Введите фамилию пользователя:");
        String firstName = scanner.nextLine();
        if (TaskManagerUtil.checkEmptyInput(firstName) && !firstName.equals(user.getFirstName()))
            user.setFirstName(firstName);
        System.out.println("Введите имя пользователя:");
        String middleName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(middleName) && !middleName.equals(user.getMiddleName()))
            user.setMiddleName(middleName);
        System.out.println("Введите отчество пользователя:");
        String secondName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(secondName) && !secondName.equals(user.getSecondName()))
            user.setSecondName(secondName);
        System.out.println("Введите новый пароль пользователя:");
        String password = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(password)) {
            TaskManagerUtil.printAndReturnFail("Пароль не может быть пустым!");
            return;
        }
        user.setPassword(password);
    }

    /**
     * Удаление пользователя
     *
     * @return код исполнения
     */
    public int deleteUser() {
        logger.info("[DELETE USER]");
        if (!getAppUser().isAdmin())
            return TaskManagerUtil.printAndReturnFail("Вы не имеете прав на удаление пользователей!");
        System.out.println("Введите регистрационное имя пользователя:");
        final String loginName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(loginName))
            return TaskManagerUtil.printAndReturnFail("Логин пользователя не моожет быть пустым!");
        userService.deleteUserByLogin(loginName);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список пользователей
     *
     * @return список пользователей в системе {@link List}
     * @see User
     */
    public int listUsers() {
        logger.info("[LIST USERS]");
        userRepository.findAll().forEach(System.out::println);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Вход в систему пользователя
     *
     * @return пользователь, залогинившийся в системе {@link User}
     */
    public int userLogin() {
        System.out.println("Введите имя пользователя:");
        final String loginName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(loginName))
            return TaskManagerUtil.printAndReturnFail("Вход в систему не удался. Имя пользователя не введено.");
        final User user = userService.getUserByloginName(loginName);
        if (user == null)
            return TaskManagerUtil.printAndReturnFail("Пользователя с таким логином не существует.");
        System.out.println("Введите пароль:");
        final String userPassword = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userPassword))
            return TaskManagerUtil.printAndReturnFail("Вход в систему не удался. Пароль не может быть пустым!");
        if (!user.getPassword().equals(TaskManagerUtil.getMD5Hash(userPassword)))
            return TaskManagerUtil.printAndReturnFail("Вход в систему не удался. Пароль не верен.");
        setAppUser(user);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Просмотр пользователя по ID
     *
     * @param userId код пользователя
     */
    public void viewUserById(final String userId) {
        if (!TaskManagerUtil.checkEmptyInput(userId)) return;
        List<User> filterList = userService.findAll().stream().filter(user1 ->
                user1.getUserId().equals(UUID.fromString(userId))).collect(Collectors.toList());
        if (filterList.isEmpty()) {
            TaskManagerUtil.printAndReturnFail("Пользователь с ID= " + userId + " не найден");
            return;
        }
        final User user = filterList.get(0);
        System.out.println(user);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Просмотр профиля пользователя
     *
     * @return код исполнения
     */
    public int viewUserProfile() {
        logger.info("[VIEW USER PROFILE]");
        if (getAppUser() == null)
            return TaskManagerUtil.printAndReturnFail("Пользователь не определен!");
        if (!getAppUser().isAdmin()) {
            viewUserById(getAppUser().getUserId().toString());
            return TaskManagerUtil.printAndReturnOk();
        }
        System.out.println("Введите ID пользователя");
        final String userId = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userId)) {
            viewUserById(getAppUser().getUserId().toString());
        } else
            viewUserById(userId);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Смена пароля пользователя
     *
     * @return код исполнения
     */
    public int changeUserPassword() {
        logger.info("[CHANGE USER PASSWORD]");
        if (getAppUser() == null) return 0;
        if (!getAppUser().isAdmin()) {
            changeUserPasswordById(getAppUser().getUserId().toString());
            return TaskManagerUtil.printAndReturnOk();
        }
        System.out.println("Введите ID пользователя");
        final String userId = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userId)) {
            changeUserPasswordById(getAppUser().getUserId().toString());
        } else
            changeUserPasswordById(userId);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Смена пароля по ID пользователя
     *
     * @param userId ID пользоваьеля
     */
    private void changeUserPasswordById(final String userId) {
        if (!TaskManagerUtil.checkEmptyInput(userId)) return;
        final User user = userService.getUserById(userId);
        if (user == null) return;
        System.out.println("Введите новый пароль:");
        final String inputPasswordString = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(inputPasswordString)) {
            TaskManagerUtil.printAndReturnFail("Пароль не может быть пустым!");
            return;
        }
        user.setPassword(inputPasswordString);
        userService.update(user);
    }

    /**
     * Редактирование профиля пользователя
     *
     * @return код исполнения
     */
    public int editUserProfile() {
        logger.info("[EDIT USER PROFILE]");
        if (getAppUser() == null) return -1;
        if (!getAppUser().isAdmin()) {
            inputFIO(getAppUser());
            userService.update(getAppUser());
            return TaskManagerUtil.printAndReturnOk();
        }
        System.out.println("Введите код пользователя");
        final String userId = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userId)) {
            inputFIO(getAppUser());
            userService.update(getAppUser());
        } else {
            final User user = userService.getUserById(userId);
            if (user == null) return -1;
            inputFIO(user);
            userService.update(user);
        }
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Отключение пользователя
     *
     * @return код исполнения
     */
    public int userLogOut() {
        logger.info("Отключение пользователя...");
        setAppUser(null);
        return TaskManagerUtil.printAndReturnOk();
    }

}
