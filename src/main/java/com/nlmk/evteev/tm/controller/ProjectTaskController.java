package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.TaskService;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Класс контроллера для связи задач/проектов
 * что бы не внедрять контроллеры задач и проектов между собой
 */
public class ProjectTaskController extends AbstractController {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final ProjectController projectController = new ProjectController(projectService);
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final TaskController taskController = new TaskController(taskService);
    private final Logger logger = LogManager.getLogger(ProjectTaskController.class);

    public ProjectTaskController() {
    }

    public ProjectController getProjectController() {
        return projectController;
    }

    public TaskController getTaskController() {
        return taskController;
    }

    /**
     * Просмотр проекта вместе с задачами
     *
     * @return код выполнения
     */
    public int viewProjectWithTasks() {
        logger.info("[VIEW PROJECT WITH TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project == null) System.out.println("Проект с таким ID не найден.");
        System.out.println(project);
        if (project.getTasks().size() > 0) {
            System.out.println("Задачи, назначенные на проект: ");
            project.getTasks().forEach(aLong -> {
                Task task = taskController.findTaskById(aLong);
                if (task != null) {
                    taskController.viewTask(task);
                }
            });
            return TaskManagerUtil.printAndReturnOk();
        } else
            return TaskManagerUtil.printAndReturnFail("К данному проекту не привязана ни одна задача.");
    }

    /**
     * Добавление задачи к проекту
     *
     * @return задача, добавленная к проекту
     */
    public int addTaskToProjectByIds() {
        logger.info("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project == null) return 0;
        System.out.println("Введите код задачи:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        if (taskId == null) return 0;
        Task task = taskController.findTaskById(taskId);
        if (task == null) return 0;
        taskRepository.addTaskToProjectByIds(projectId, taskId);
        projectController.addTaskToProject(projectId, taskId);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта по коду
     *
     * @return код исполнения
     */
    public int removeTaskFromProjectById() {
        logger.info("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        System.out.println("Введите код задачи:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        if (taskId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project == null) return 0;
        Task task = taskController.findTaskById(taskId);
        if (task == null) return 0;
        taskController.removeTaskFromProject(taskId);
        projectController.removeTaskFromProject(projectId, taskId);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка задач проекта
     *
     * @return код исполнения
     */
    public int clearProjectTasks() {
        logger.info("[CLEAR PROJECT TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project.getTasks().size() > 0) {
            project.getTasks().forEach(aLong -> {
                Task task = taskController.findTaskById(projectId);
                if (task != null) {
                    taskController.removeTaskFromProject(aLong);
                }
            });
            project.getTasks().clear();
        }
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление проекта вместе с задачами
     *
     * @return код исполнения
     */
    public int removeProjectWithTasks() {
        logger.info("[REMOVE PROJECT WITH TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project.getTasks().size() > 0) {
            project.getTasks().forEach(aLong -> {
                Task task = taskController.findTaskById(projectId);
                if (task != null) {
                    taskService.removeById(aLong);
                }
            });
            projectService.removeById(projectId);
        }
        return TaskManagerUtil.printAndReturnOk();
    }

}
